use std::f64::consts::PI;
use std::fmt;

pub struct Ellipse {
    width: f64,
    height: f64,
}

impl Ellipse {
    pub fn new(width: f64, height: f64) -> Self {
        Ellipse { width, height }
    }

    pub fn area(&self) -> f64 {
        area_ellipse (self.width, self.height)
    }
}

fn area_ellipse(width: f64, height: f64) -> f64 {
    width / 2.0 * height / 2.0 * PI
}

pub struct Circle {
    radius: u32,
}

impl Circle {
    pub fn new(radius: u32) -> Self {
        Circle { radius }
    }

    pub fn area(&self) -> u32 {
        area_circle(self.radius) as u32
    }
}

fn area_circle(radius: u32) -> f64 {
    let d = (radius as f64) * 2.0;
    area_ellipse(d, d)
}

impl fmt::Display for Circle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Круг радиусом {} имеет площадь {}", self.radius, self.area())
    }
}

pub struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    pub fn new(width: u32, height: u32) -> Self {
        Rectangle { width, height }
    }
    pub fn area(&self) -> u32 {
        area_rectangle(self.width as f64,
                       self.height as f64) as u32
    }
}

fn area_rectangle(width: f64, height: f64) -> f64 {
    width * height
}

impl fmt::Display for Rectangle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Прямоугольник со сторонами {} {} имеет площадь {}",
               self.height, self.width, self.area())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t_area_circle() {
        assert_eq!(area_circle(6), 113.09733552923255);
        // area::calc_area_out();
        // assert_eq!(area::square_area(9), 81);
    }

    #[test]
    fn t_ellipse() {
        assert_eq!(area_ellipse(10.0, 20.0),
                   157.07963267948966);
    }

    #[test]
    fn t_rectangle() {
        assert_eq!(area_rectangle(10.0, 20.0),
                   200.0);
    }

    #[test]
    fn t_rectangle_st() {
        let rect1 = Rectangle {
            width: 30,
            height: 50,
        };
        assert_eq!(rect1.area(),1500);
    }
}
