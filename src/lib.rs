use std::fmt;
use area::*;
use volume::*;

pub enum Shape {
    Rectangle,
    Circle,
    Parallelepiped,
    Sphere,
}
pub mod area;

pub mod volume;
