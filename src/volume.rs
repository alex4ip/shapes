use std::f64::consts::PI;
use std::fmt;

pub struct Ellipsoid {
    width: f64,
    height: f64,
    length: f64,
}

impl Ellipsoid {
    pub fn new(width: f64, height: f64, length: f64) -> Self {
        Ellipsoid { width, height, length }
    }

    pub fn volume(&self) -> f64 {
        volume_ellipsoid(self.width, self.height, self.length)
    }
}

fn volume_ellipsoid(width: f64, height: f64, length: f64) -> f64 {
    width * height * length / 6.0 * PI
}

pub struct Sphere {
    radius: u32,
}

impl Sphere {
    pub fn new(radius: u32) -> Self {
        Sphere { radius }
    }

    pub fn volume(&self) -> u32 {
        volume_sphere(self.radius) as u32
    }
}

fn volume_sphere(radius: u32) -> f64 {
    let d = (radius as f64) * 2.0;
    volume_ellipsoid(d, d, d)
}

impl fmt::Display for Sphere {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Сфера радиусом {} имеет объем {}", self.radius, self.volume())
    }
}

pub struct Parallelepiped {
    width: u32,
    height: u32,
    length: u32,
}

impl Parallelepiped {
    pub fn new(width: u32, height: u32, length: u32) -> Self {
        Parallelepiped { width, height, length }
    }
    pub fn volume(&self) -> u32 {
        volume_parallelepiped(self.width as f64,
                              self.height as f64,
                              self.length as f64) as u32
    }
}

fn volume_parallelepiped(width: f64, height: f64, length: f64) -> f64 {
    width * height * length
}

impl fmt::Display for Parallelepiped {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Параллелепипед со сторонами {} {} {} имеет объем {}",
               self.height, self.width, self.length, self.volume())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t_volume_sphere() {
        assert_eq!(volume_sphere(6), 904.7786842338604);
    }

    #[test]
    fn t_ellipsoid() {
        assert_eq!(volume_ellipsoid(10.0, 20.0, 30.0),
                   3141.592653589793);
    }

    #[test]
    fn t_parallelepiped() {
        assert_eq!(volume_parallelepiped(10.0, 20.0, 30.0),
                   6000.0);
    }

    #[test]
    fn t_parallelepiped_st() {
        let parlp = Parallelepiped {
            width: 30,
            height: 50,
            length: 10,
        };
        assert_eq!(parlp.volume(), 15000);
    }
}
